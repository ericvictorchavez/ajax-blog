For this project, go to GitLab and create a new project name ‘ajax-blog’.(make sure you make this project public) Once created, clone the repo and Get from Version Control on IntelliJ. Run your git commands and connect IntelliJ to GitLab.
1. Create a new html file call ‘my-ajax-blog’
2. At minimum, your html file should have an empty <div> with an id of ‘my-posts’
3. Add Bootstrap to your html, along with custom CSS if necessary.
4. Create a new json file call ‘my-ajax-posts.json’ in your data directory
5. Select a topic/subject for you blog’s content. Add data to your json file that reflects your topic/subject (your objects should have the following properties: “title”, “content”, an array for “categories”, and “date”);
6. Use Ajax to load the data from your json file and render it to your #my-posts div
7. This is your blog, add as much content as your want, make it as formal as you can.
Collapse
