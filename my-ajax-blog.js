"use strict"
$(document).ready(function(){
    $.ajax({
        url: 'data/my-ajax-posts.json',
        type: 'GET',
        dataType: 'JSON',
        success: function(response){
            var len = response.length;
            for(var i=0; i<len; i++){
                var title = response[i].title;
                var content = response[i].content;
                var categories = response[i].categories;
                var date = response[i].date;
                // var tr_str = "<tr>" +
                //
                //     "<td align='center'>" + title + "</td>" +
                //     "<td align='center'>" + content + "</td>" +
                //     "<td align='center'>" + categories + "</td>" +
                //     "<td align='center'>" + date + "</td>" +
                //     "</tr>";

                var blog = "<div class='card text-center'>" +
            "<div class='card-header'>" + title + "</div>" +
                    "<div class='card-body'>" +
                    "<h5 class='card-title'>" + content  + "</h5>" +
                    "<p class='card-text'>" + categories + "</p>" +
                    "</div>" +
                "<div class='card-footer text-muted'>" + date + "</div>" +
                    "<br>" +
                    "<br>"


                $("#my-posts").append(blog);
            }
        }
    });
});
